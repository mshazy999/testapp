import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Cmp from "./Solutions/Cmp";

function App() {
  return (
    <div className="App text-center">
      <Cmp />
    </div>
  );
}

export default App;
