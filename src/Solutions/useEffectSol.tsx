import React, { useEffect, useState } from "react";

const UseEffecSol = () => {
  const [val, setVal] = useState<string>(() =>
    JSON.parse(localStorage.getItem("text")!)
  );
 
  const handleChage = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setVal(value);
    localStorage.setItem("text", JSON.stringify(value));
  };
  console.log("rende3r");
  return (
    <div>
      <input type="text" name="" id="" onChange={handleChage} />
      <h2>Your Value: {val}</h2>
    </div>
  );
};

export default UseEffecSol;
