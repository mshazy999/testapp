import React, { useState } from "react";

const StateSol = () => {
  const [val, setVal] = useState<string>("");
  return (
    <div>
      <input
        type="text"
        name=""
        id=""
        onChange={(e) => setVal(e.target.value)}
      />
      <h2>{val}</h2>
    </div>
  );
};

export default StateSol;
