import React, { useEffect, useState } from "react";
interface pokemonType {
  name: string;
  url: string;
}
interface responseType {
  results: [pokemonType];
}
const Pokemon = () => {
  const [text, setText] = useState<string>();
  const [response, setResponse] = useState<any>();
  const url = "https://pokeapi.co/api/v2/pokemon?limit=500";
  const fetchData = async () => {
    try {
      const responsData = await fetch(url);
      const { results } = await responsData.json();
      console.log(results);
      
      setResponse(results);
    } catch (error) {
      console.log(error.message);
    }
  };
  useEffect(() => {
    fetchData();
  }, [text]);
//   const handleChange = (e: string) => {
//     setText(e);
//   };
  return (
    <div>
      <h1>Pokemon Finder</h1>
      <input
        // onChange={(e) => handleChange(e.target.value)}
        className="mt-1"
        type="text"
        placeholder="Enter Name..."
      />
      {response?.map((pokemon: { name: string; url: string }) => (
        <div>
          <img
            className="mt-1"
            src={pokemon?.url}
            alt=""
            style={{ height: "300px", width: "300px" }}
          />
          <p>{pokemon?.name}</p>
        </div>
      ))}
    </div>
  );
};

export default Pokemon;
